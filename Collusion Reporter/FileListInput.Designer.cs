﻿namespace Collusion_Reporter
{
    partial class FileListInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.InputListBtn = new System.Windows.Forms.Button();
            this.InputListLabel = new System.Windows.Forms.Label();
            this.InputListTxBx = new System.Windows.Forms.TextBox();
            this.ReportBtn = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ReportBtn);
            this.splitContainer1.Panel1.Controls.Add(this.InputListTxBx);
            this.splitContainer1.Panel1.Controls.Add(this.InputListLabel);
            this.splitContainer1.Panel1.Controls.Add(this.InputListBtn);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.webBrowser1);
            this.splitContainer1.Size = new System.Drawing.Size(778, 544);
            this.splitContainer1.SplitterDistance = 259;
            this.splitContainer1.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // InputListBtn
            // 
            this.InputListBtn.Location = new System.Drawing.Point(12, 73);
            this.InputListBtn.Name = "InputListBtn";
            this.InputListBtn.Size = new System.Drawing.Size(132, 28);
            this.InputListBtn.TabIndex = 0;
            this.InputListBtn.Text = "Locate Folder";
            this.InputListBtn.UseVisualStyleBackColor = true;
            // 
            // InputListLabel
            // 
            this.InputListLabel.AutoSize = true;
            this.InputListLabel.Location = new System.Drawing.Point(12, 9);
            this.InputListLabel.Name = "InputListLabel";
            this.InputListLabel.Size = new System.Drawing.Size(155, 20);
            this.InputListLabel.TabIndex = 1;
            this.InputListLabel.Text = "Source Code Folder:";
            // 
            // InputListTxBx
            // 
            this.InputListTxBx.Location = new System.Drawing.Point(12, 32);
            this.InputListTxBx.Name = "InputListTxBx";
            this.InputListTxBx.Size = new System.Drawing.Size(240, 26);
            this.InputListTxBx.TabIndex = 2;
            // 
            // ReportBtn
            // 
            this.ReportBtn.Location = new System.Drawing.Point(27, 501);
            this.ReportBtn.Name = "ReportBtn";
            this.ReportBtn.Size = new System.Drawing.Size(211, 31);
            this.ReportBtn.TabIndex = 3;
            this.ReportBtn.Text = "Create Collusion Report";
            this.ReportBtn.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(515, 544);
            this.webBrowser1.TabIndex = 0;
            // 
            // FileListInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 544);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FileListInputForm";
            this.Text = "FileListInputForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button ReportBtn;
        private System.Windows.Forms.TextBox InputListTxBx;
        private System.Windows.Forms.Label InputListLabel;
        private System.Windows.Forms.Button InputListBtn;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

